#include "stdafx.h"
#include "ScreenBuffer.h"


#ifndef _SCREEN_BUFFER_CPP_
#define _SCREEN_BUFFER_CPP_

//------------------------------------------------------------
// Constructors / Desstructor
//------------------------------------------------------------
ScreenBuffer::ScreenBuffer(void)
{
	_rows = -1;
	_cols = -1;
}


ScreenBuffer::~ScreenBuffer(void)
{
}

//------------------------------------------------------------
// Member Methods
//------------------------------------------------------------
/**
* setBounds(rows, cols)
*   - rows : int, max number of rows
*   - cols : int, max number of cols
*   Sets the maximum size.
*/
void ScreenBuffer::setBounds(int rows, int cols)
{
	this->setBounds(rows, cols, 0);
}

/**
* setBounds(rows, cols, initialValue)
*   - rows         : int, max number of rows
*   - cols         : int, max number of cols
*   - initialValue : initializing value
*   Sets the maximum size an initializes every element.
*/void ScreenBuffer::setBounds(int rows, int cols, char initialValue)
{
	//store size
	_rows = rows;
	_cols = cols;
	_data.resize(rows * cols, initialValue); //set the bounds of the vector, initialize to 0
}

/**
* set(row, col, c)
*   - row : int, row to set
*   - col : int, column to set
*   - c   : value to set it to
*   Sets the value.
*/
void ScreenBuffer::set(int row, int col, char c) 
{
	//make sure row and col are in bounds
	if ((row >= 0 && row < _rows) && 
		(col >= 0 && col < _cols))
	{
		_data[indexOf(row, col)] = c;
	}
}

/**
* get(row, col)
*   - row : int, row to get
*   - col : int, column to get
*   Returns the value at the location given
*/
char ScreenBuffer::get(int row, int col) const
{
	//make sure data is set and row / col are in bounds
	if ((row >= 0 && row < _rows) && 
		(col >= 0 && col < _cols))
	{
		return char(_data[indexOf(row, col)]);
	}
	return -1; // bad value... could be handled better, but...
} 

/**
* getLine(row)
*   - row : int, row to get
*   Gets a string containing everything in a given row. Used to help in rendering.
*/
std::string ScreenBuffer::getLine(int row) 
{
	std::string line;
	if (row >= 0 && row < _rows)
	{
		std::stringstream ss;
		for (int col = 0; col < _cols; col++)
		{
			//copy line char by char
			ss << _data[indexOf(row, col)]; 
		}
		//copy the final result into return value
		line = ss.str();
	}

	return line; // if row is out of bounds, line is empty string
}

/**
* indexOf(row, col)
*   - rows : int, desired row
*   - cols : int, desired col
*   Returns a single integer value of the row, col. This maps a 2-d matrix into a 1-d array.
*/
int ScreenBuffer::indexOf(int row, int col) const
{
	if (_rows > 0 && _cols > 0)
		return (_cols * row) + col;
	else
		return -1; //problems.
}

/**
* getRows()
*   Gets the max number of rows.
*/
int ScreenBuffer::getRows() const { return _rows; }
/**
* getCols()
*   Gets the max number of columns.
*/
int ScreenBuffer::getCols() const { return _cols; }

#endif