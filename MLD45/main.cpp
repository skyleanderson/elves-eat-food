// entry point for the console application.
//

#include "stdafx.h"

#include "Game.h"

//------------------------------------------------------------
// MAIN
//   This program is object-oriented. The Game object contains all logic, so the main
//   function just needs to create a game and run it.
//------------------------------------------------------------
int main(int argc, char* argv[])
{
	Game g;

	g.run();

	return 0;
}

