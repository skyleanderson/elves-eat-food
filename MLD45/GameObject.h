#ifndef _GAMEOBJECT_H_
#define _GAMEOBJECT_H_

//------------------------------------------------------------
//  GameObject class
//    This is simply an interface (pure abstract class) used for
//    any object in the game. It provides the interface used by
//    the Game class to execute the game loop.
//------------------------------------------------------------
class GameObject
{
public:
	GameObject(void);
	~GameObject(void);

	virtual void load() = 0;
	virtual void update() = 0;
	virtual void draw() = 0;
	virtual void unload() = 0;
};

#endif