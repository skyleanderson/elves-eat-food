#include "Position.h"
#include "ScreenBuffer.h"
#include "GameWorld.h"

#ifndef _PATH_H_
#define _PATH_H_

//------------------------------------------------------------
//  Path class
//    A Path is a list of steps for a Creature to follow. It also includes
//    a factory method to generate a path with the A* algorithm
//------------------------------------------------------------
class Path
{
public:
	Path(void);
	~Path(void);

	std::vector<Position> steps;

	//static
	static void GetPathAStar(Path & result, const Position & to, const Position & from, const ScreenBuffer & worldState);

private:
	struct PathNode
	{
		Position position; //position of this node
		Position previous; //previous position for reconstructing path... should probably be a pointer...

		//for a* algorithm, F = G + H where 
		//            F is total expected cost to the goal from a node
		//            G is the distance from the origin to this node
		//            H is the heuristic guess of the distance from this node to the end
		//in our implementation, 'lengthToHere' = G, and 'estimatedLenthToGoal' = H, which is calculated by the GameWorld::distanceTo helper
		int lengthToHere;  //how far is this from the starting point
		int estimatedLenghtToGoal; //direct line distance to ending goal
	};

	static bool IsInList(const PathNode &, const std::vector<PathNode> &);
	static int FindBestNodeInOpenSet(const std::vector<PathNode> &);
};

#endif
