#ifndef _POSITION_H_
#define _POSITION_H_

//------------------------------------------------------------
//  Position class
//    Simple class to hold an x, y position of an item.
//------------------------------------------------------------
class Position
{
public:
	Position(void);
	Position(int, int);
	~Position(void);

	//Constants
	static const Position UP;
	static const Position DOWN;
	static const Position LEFT;
	static const Position RIGHT;

	//member variables
	int x;
	int y;

	//member methods
	void set(int, int);

	void add(const Position&);
	void subtract(const Position&);
	int distanceTo(const Position&) const;
};

#endif