#include "stdafx.h"
#include "Creature.h"

#ifndef _CREATURE_CPP_
#define _CREATURE_CPP_

//------------------------------------------------------------
//  Constructors / Destructor
//------------------------------------------------------------
Creature::Creature(void)
{
}


Creature::~Creature(void)
{
}

//------------------------------------------------------------
//  GameObject Interface implementations
//------------------------------------------------------------
/**
* update()
*   does nothing, force to use the overload
*/
void Creature::update() {} 


/**
* update(world, food)
*   - world: ScreenBuffer containing the game state, it is passed around to helper functions for AI
*   - food : Vector of Positions, the locations of food on the map, passed to help with AI
*   Overrides GameObject::update() with basic update flow
*/
void Creature::update(const ScreenBuffer & world, const std::vector<Position> & food)
{
	//only act if alive
	if (!this->isDead())
	{
		//if an action is set and valid, keep doing it
		//if there is no current action, see if you need a new action
		this->chooseAction(world, food);

		//follow the new current action
		switch (this->state)
		{
		case Creature::WANDER:
			this->performWander(world, food);
			break;
		case Creature::FOLLOW_PATH:
			this->performFollowPath(world, food);
			break;
		case Creature::PATROL:
			this->performPatrol(world, food);
		default:
			this->performNone(world, food);
			break;
		}

		//take damage from hunger
		if (this->hunger == 0) 
			this->health--;

		//get more hungry
		this->hunger--;
		if (this->hunger < 0) this->hunger = 0;

	}
}

//------------------------------------------------------------
//  Member Methods
//------------------------------------------------------------
/**
* isDead()
*   Returns true if the creature's life is 0.
*/
bool Creature::isDead()
{
	return (this->health <= 0);
}

/**
* damage(value)
*   - value: int with the amount of damage.
*   Damages the Creature, currently not used.
*/
void Creature::damage(int value)
{
	this->health -= value; //no error checking for now
	if (this->health < 0) this->health = 0;
}

//------------------------------------------------------------
//  Creature Virtual Method Definitions
//------------------------------------------------------------
//creature interface for performing actions
//default behavior is to do nothing
void Creature::chooseAction(const ScreenBuffer & world, const std::vector<Position> & food) {}
void Creature::performNone(const ScreenBuffer & world, const std::vector<Position> & food) {}
void Creature::performWander(const ScreenBuffer & world, const std::vector<Position> & food) {}
void Creature::performFollowPath(const ScreenBuffer & world, const std::vector<Position> & food) {}
void Creature::performPatrol(const ScreenBuffer & world, const std::vector<Position> & food) {}

#endif
