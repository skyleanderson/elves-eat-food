
#include "stdafx.h"
#include "Position.h"

#ifndef _POSITION_CPP_
#define _POSITION_CPP_


//------------------------------------------------------------
// Constants
//------------------------------------------------------------
const Position Position::UP(0, -1);
const Position Position::DOWN(0, 1);
const Position Position::LEFT(-1, 0);
const Position Position::RIGHT(1, 0);

//------------------------------------------------------------
//  Constructors / Destructor
//------------------------------------------------------------
Position::Position(void)
{
	Position(0,0);
}

Position::Position(int x, int y)
{
	this->set(x, y);
}


Position::~Position(void)
{
}

//------------------------------------------------------------
//  Member Methods
//------------------------------------------------------------
/**
* set(x, y)
*   - x : int, x value
*   - y : int, y value
*   Assigns x and y
*/
void Position::set(int x, int y) 
{
	this->x = x;
	this->y = y;
}

/**
* add(rhs)
*   - rhs: Position, values to add
*   Adds rhs to this.
*/
void Position:: add(const Position& rhs) 
{
	this->x += rhs.x;
	this->y += rhs.y;
}

/**
* subtract(rhs)
*   - rhs: Position, values to subtract
*   Subtracts rhs from this.
*/
void Position:: subtract(const Position& rhs) 
{
	this->x -= rhs.x;
	this->y -= rhs.y;
}

/**
* distanceTo(target)
*   - target : Position, values to subtract
*   Returns the distance to the target Position. Since all values are integers, this uses the taxicab metric.
*/
int Position:: distanceTo(const Position& target) const
{
	int xpart = target.x - this->x;
	if (xpart < 0) xpart *= -1;

	int ypart = target.y - this->y;
	if (ypart < 0) ypart *= -1;
	return (xpart + ypart);
}

#endif