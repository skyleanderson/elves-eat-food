
#include "stdafx.h"
#include "GameWorld.h"

#ifndef _GAMEWORLD_CPP_
#define _GAMEWORLD_CPP_


//------------------------------------------------------------
//  Constants
//------------------------------------------------------------
const int GameWorld::WORLD_WIDTH  = 70;
const int GameWorld::WORLD_HEIGHT = 20;

//------------------------------------------------------------
//  Constructors / Destructor
//------------------------------------------------------------
GameWorld::GameWorld(void)
{
}


GameWorld::~GameWorld(void)
{
}


//------------------------------------------------------------
//  Static Methods
//------------------------------------------------------------
/**
* TerrainSteps()
*   - state   : ScreenBuffer, the terrain to test agains
*   - location: specific terrain location
*   Returns the number of updates needed to walk across the given terrain
*/
GameWorld::WALKING_STEPS GameWorld::TerrainSteps(const ScreenBuffer & state, const Position & location)
{
	unsigned char val = state.get(location.y, location.x);
	if (val == GameWorld::LAND || val == GameWorld::TREASURE || val == GameWorld::FOOD)
		//land, treasure, and food are walkable
		return GameWorld::WALKABLE;
	else if (val == GameWorld::TREE)
		//trees are obstructed, take two turns to walk through
		return GameWorld::OBSTRUCTED;
	else 
		//includes walls and other creatures
		return GameWorld::UNWALKABLE; 
}

/**
* IsPositionInBounds()
*   - location: the location to test
*   Returns true if the given location is within the world bounds
*/
bool GameWorld::IsPositionInBounds(const Position & location)
{
	return ( (location.x >=0 && location.x < GameWorld::WORLD_WIDTH) &&
		     (location.y >=0 && location.y < GameWorld::WORLD_HEIGHT) );
}

//------------------------------------------------------------
//  GameObject Interface Implementations
//-----------------------------------------------------------
/**
* update()
*   Generates random terrain
*/
void GameWorld::load() 
{
	//initialize whole world to land
	this->terrain.setBounds(GameWorld::WORLD_HEIGHT, GameWorld::WORLD_WIDTH, GameWorld::LAND);
	//this->terrain.resize(GameWorld::WORLD_HEIGHT * GameWorld::WORLD_WIDTH, GameWorld::LAND);
	
	//random trees
	for (int row = 0; row < GameWorld::WORLD_HEIGHT; row++)
		for (int col = 0; col < GameWorld::WORLD_WIDTH; col++)
			if (rand() % 20 > 18)
				this->terrain.set(row, col, char(GameWorld::TREE));

	//spawn unwalkable terrain
	int max = (rand() % 7) + 3;
	for (int i = 0; i < max; i++)
	{
		int x = rand() % GameWorld::WORLD_WIDTH;
		int y = rand() % GameWorld::WORLD_HEIGHT;
		int size = (rand()% 5) + 2;
		for (int row = y - size; row < y + size; row++)
		{
			for (int col = x - size; col < x + size; col++)
			{
				if (!(row == y - size && col == x - size) &&
					!(row == y + size - 1 && col == x - size) && 
					!(row == y - size && col == x + size - 1) &&
					!(row == y + size - 1 && col == x + size - 1))
					if (GameWorld::IsPositionInBounds(Position(col, row)))
						terrain.set(row, col, GameWorld::WALL);
			}
		}
	}
}

/**
* update()
*   Not used.
*/
void GameWorld::update()
{
	this->update("");
}

/**
* update(input)
*   Not used
*/
void GameWorld::update(std::string input) 
{
}

/**
* draw()
*   Not used. Use the override
*/
void GameWorld::draw() 
{
}

/**
* update(buffer)
*   - buffer: ScreenBuffer, the buffer to draw to.
*   Draws the world terrain to the given buffer.
*/
void GameWorld::draw(ScreenBuffer & buffer)
{
	//fill the buffer with the terrain
	for (int row = 0; row < GameWorld::WORLD_HEIGHT; row++)
	{
		for (int col=0; col < GameWorld::WORLD_WIDTH; col++)
		{
			buffer.set(row, col, terrain.get(row, col));
		}
	}

}

/**
* unload()
*   Not used.
*/
void GameWorld::unload() {}


//------------------------------------------------------------
//  Member Methods
//------------------------------------------------------------
/**
* get(row, col)
*   - row: int, y-value
*   - col: int, x-value
*   Returns the terrain at the given location
*/
char GameWorld::get(int row, int col) const
{
	//last resort, get terrain
	return char(this->terrain.get(row, col));
}

/**
* getRandomPosition()
*   Returns an open position on the map. This is needed to make sure food and creatures don't
*   spawn in the middle of an unwalkable area.
*/
Position GameWorld::getRandomOpenPosition()
{
	int spot = rand() % (GameWorld::WORLD_WIDTH * GameWorld::WORLD_HEIGHT);
	int i = -1;
	int row = 0, col = 0;

	while (i < spot)
	{
		unsigned char t = terrain.get(row, col);
		if (t == GameWorld::LAND || t == GameWorld::TREE || t == GameWorld::FOOD) i++;

		col++;
		if (col >= GameWorld::WORLD_WIDTH)
		{
			row++;
			col = 0;
		}

		if (row >= GameWorld::WORLD_HEIGHT) row = 0;
	}

	return Position(col, row);
}

#endif