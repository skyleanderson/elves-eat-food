#include "stdafx.h"
#include "Path.h"

#ifndef _PATH_CPP_
#define _PATH_CPP_

//------------------------------------------------------------
//  Constructors / Destructor
//------------------------------------------------------------
Path::Path(void)
{
}


Path::~Path(void)
{
}

//------------------------------------------------------------
//  Static Methods
//------------------------------------------------------------
/**
* GetPathAStar(result, to, from, worldState)
*   - result : Path [OUTPUT], the path will be generated here!
*   - to: Position, the goal location
*   - from: Position, the starting position
*   - worldState: ScreenBuffer, the map to find a path on with everything on it
*   
*   Runs the A* algorithm (my implementation is REALLY bad, needs a lot of optiimizations). The result Path will be
*   cleared and filled with the newly generated path. This A* implementation has been given a little bit of random
*   variance (i.e. at a given step, multiple options have the same expected cost, one is chosen at random).
*/
void Path::GetPathAStar(Path & result, const Position & to, const Position & from, const ScreenBuffer & worldState) 
{
	//empty the path
	while (!result.steps.empty()) result.steps.pop_back();

	//1. Check if a path needs to be generated at all
	//1.a  to == from
	if (to.x == from.x && to.y == from.y) return;

	//1.b to is unwalkable, no path
	if (GameWorld::TerrainSteps(worldState, to) == GameWorld::UNWALKABLE) return;
	
	//2. Create open and closed sets
	std::vector<PathNode> open;   //nodes to check
	std::vector<PathNode> closed; //nodes that have already been checked, along with their parent

	//3. add the starting point ot the open set
	PathNode original;
	original.lengthToHere = 0;
	original.estimatedLenghtToGoal = from.distanceTo(to);
	original.position.x = from.x;
	original.position.y = from.y;
	original.previous.x = -1;
	original.previous.y = -1;
	open.push_back(original);

	//4. start the pathfinding loop
	while (!open.empty())
	{
		//get the node in the open set with the lowest expected cost
		int bestIndex = FindBestNodeInOpenSet(open);
		PathNode current = open[bestIndex];
		open.erase(open.begin() + bestIndex); //erase the chosen node from the open list

		//test if done
		if (current.position.x == to.x && current.position.y == to.y)
		{
			closed.push_back(current);
			break; //break while(!open.empty);
		}

		//create nodes for all possible children
		PathNode children[4] = {current, current, current, current};
		children[0].position.y -= 1;
		children[1].position.y += 1;
		children[2].position.x -= 1;
		children[3].position.x += 1;

		for (int i = 0; i < 4; i++ )
		{
			//set the previous of the children to the current
			children[i].previous.x = current.position.x;
			children[i].previous.y = current.position.y;
			
			//if child is in bounds 
			//   and the position is walkable
			//   and not already in the open or closed list, add it to the list
			if (GameWorld::IsPositionInBounds(children[i].position) &&
				GameWorld::TerrainSteps(worldState, children[i].position) != GameWorld::UNWALKABLE &&
				!IsInList(children[i], open) &&
				!IsInList(children[i], closed))
			{
				//set the distance travelled to get here
				children[i].lengthToHere = current.lengthToHere + GameWorld::TerrainSteps(worldState, children[i].position);
			
				//set the heuristic guess of how far there is to go
				children[i].estimatedLenghtToGoal = to.distanceTo(children[i].position);

				//add to open set
				open.push_back(children[i]);
			} 
		}

		//new nodes have been added to open, let's move the current one into closed
		closed.push_back(current);            //add the chosen node to the closed
	}

	//5. reconstruct path
	Position prevStep;
	int i = closed.size()-1; //start and the end of the list
	do
	{
		//add current position
		result.steps.insert(result.steps.begin(), Position(closed[i].position.x, closed[i].position.y));

		//find the previous position in the list
		prevStep.x = closed[i].previous.x;
		prevStep.y = closed[i].previous.y;
		do 
		{
			i--;
		} while (i > 0 &&
			 	 (closed[i].position.x != prevStep.x ||
				 closed[i].position.y != prevStep.y));
	}  while (i > 0);
}

/**
* IsInList(node, list)
*   - node : PathNode, the node to look for
*   - list : Vector of PathNodes, a set of nodes to look for 'node' in.
*   Helper for the A* algorithm.
*   Searches for 'node' in 'list'. This needs optimization, just does a linear search every time.
*/
bool Path::IsInList(const PathNode & node, const std::vector<PathNode> & list)
{
	for (unsigned int i=0; i<list.size(); i++)
	{
		if (node.position.x == list[i].position.x &&
			node.position.y == list[i].position.y)
			return true;
	}
	return false;
}

/**
* FindBestNodeInOpenSet(open)
*   - open : Vector of PathNodes, the set to choose a node from
*   Helper for A* algorithm.
*   Searches for the node in the 'open' list that has the lowest expected cost. This needs optimization,
*   it just loops through every value in 'open' to find the best.
*/
int Path::FindBestNodeInOpenSet(const std::vector<PathNode> & open)
{
	//base case
	if (open.size() == 1) return 0;

	int result = 0;
	int least = open[0].lengthToHere + open[0].estimatedLenghtToGoal;
	for (unsigned int i=1; i<open.size(); i++)
	{
		if (open[i].lengthToHere + open[i].estimatedLenghtToGoal < least)
		{
			least = open[i].lengthToHere + open[i].estimatedLenghtToGoal;
			result = i;
		} else if (open[i].lengthToHere + open[i].estimatedLenghtToGoal == least)
		{
			//if two paths are equal, flip a coin
			if ((rand() % 2 )== 0)
				result = i;
		}
	}
	return result;
}

#endif