#include "stdafx.h"
#include "Elf.h"


#ifndef _ELF_CPP_
#define _ELF_CPP_

//------------------------------------------------------------
//  Constants
//------------------------------------------------------------
const char Elf::DISPLAY_CHAR = 'E';
const int  Elf::BASE_HEALTH  = 100;
const int  Elf::BASE_HUNGER  = 20;
const int  Elf::HUNGER_LIMIT = 15;

//------------------------------------------------------------
//  Constructors / Destructor
//------------------------------------------------------------
Elf::Elf(void)
{
}


Elf::~Elf(void)
{
}

//------------------------------------------------------------
//  GameObject Interface implementations
//------------------------------------------------------------
/**
* load()
*   Just used for initialization.
*/
void Elf::load() 
{
	this->health = BASE_HEALTH;
	this->hunger = BASE_HUNGER;
	this->state = WANDER;
	this->stepProgress = 0;
}


/**
* update(world, food)
*   - world: ScreenBuffer containing the game state, it is passed around to helper functions for AI
*   - food : Vector of Positions, the locations of food on the map, passed to help with AI
*   Just calls the Creature version, then does a little cleanup to restore health
*/
void Elf::update(const ScreenBuffer & world, const std::vector<Position> & food) 
{
	Creature::update(world, food); //define actions with the creature interface
	if (this->health < Elf::BASE_HEALTH && this->hunger > 0) this->health++;
} 

/**
* draw()
*   Not used; use the override
*/
void Elf::draw() {} // use the override

/**
* draw(buffer)
*   Draws a character in the Elf's position. If it is following a path, it also draws that to the buffer.
*/
void Elf::draw(ScreenBuffer & buffer) 
{
	if (!this->isDead())
	{
		buffer.set(this->position.y, this->position.x, Elf::DISPLAY_CHAR);

		if (state == FOLLOW_PATH)
		{
			//draw the path
			for (unsigned int i=0; i < currentPath.steps.size(); i++)
				if (buffer.get(currentPath.steps[i].y, currentPath.steps[i].x) != 'E')
					buffer.set(currentPath.steps[i].y, currentPath.steps[i].x, '.');
		}
	}
}

/**
* unload()
*   Not used
*/void Elf::unload() {} //do nothing

//------------------------------------------------------------
//  Member Methods
//------------------------------------------------------------
/**
* eat()
*   Restores the Elf's hunger meter. If it was following a path, the path is cleared and the Elf Wanders.
*/
void Elf::eat()
{
	//fill up
	this->hunger = Elf::BASE_HUNGER;
	//clear current path
	while (!currentPath.steps.empty())
		currentPath.steps.pop_back();
	//go back to wandering
	state = WANDER;
}

/**
* findFood(world, food)
*   - world: ScreenBuffer containing the game state, it is passed around to helper functions for AI
*   - food : Vector of Positions, the locations of food on the map, passed to help with AI
*   Loops through the food list to find the cloasest food. Then, it finds a path to the
*   food and begins to follow the path.
*/
void Elf::findFood(const ScreenBuffer & world, const std::vector<Position> & food)
{
	if (food.size() > 0)
	{
		//find food and go there
		int closestIndex = 0;
		int closestDistance = 99999;
		for (int i = 0; i < food.size(); i++)
		{
			if (food[i].distanceTo(this->position) < closestDistance)
			{
				closestDistance = food[i].distanceTo(this->position);
				closestIndex = i;
			}
		}

		//closest food found, set path
		Path::GetPathAStar(this->currentPath, food[closestIndex], this->position, world);
		this->state = Creature::FOLLOW_PATH;
	} else
	{
		//no food, wander around
		state = Creature::WANDER;
	}
}


//------------------------------------------------------------
//  Creature Interface implementations
//------------------------------------------------------------
/**
* chooseAction(world, food)
*   - world: ScreenBuffer containing the game state, it is passed around to helper functions for AI
*   - food : Vector of Positions, the locations of food on the map, passed to help with AI
*   Based on the current action state, it decides what it should do. In general, the same state
*   is kept. If the Elf finishes a path or becomes hungry, it will find a path to food.
*/
void Elf::chooseAction(const ScreenBuffer & world, const std::vector<Position> & food) 
{

	switch (state)
	{
	case Creature::NONE:
	case Creature::WANDER:
	case Creature::PATROL:
		state = WANDER;
		
		if (hunger < Elf::HUNGER_LIMIT) 
			findFood(world, food);	

		break;
	case Creature::FOLLOW_PATH:
		state = Creature::FOLLOW_PATH;
		if (this->currentPath.steps.size() == 0 && hunger < Elf::HUNGER_LIMIT) 
			findFood(world, food);	
		break;
	default:
		state = WANDER;
		break;
	}

}

/**
* performNone(world, food)
*   - world: ScreenBuffer containing the game state, it is passed around to helper functions for AI
*   - food : Vector of Positions, the locations of food on the map, passed to help with AI
*   Not used.
*/
void Elf::performNone(const ScreenBuffer & world, const std::vector<Position> & food) {}  // do nothing

/**
* performWander(world, food)
*   - world: ScreenBuffer containing the game state, it is passed around to helper functions for AI
*   - food : Vector of Positions, the locations of food on the map, passed to help with AI
*   Picks a random direction, and tries to move there. If the new location is in a tree. It makes
*   a 1-step path to ensure that it continues going in that direction.
*/
void Elf::performWander(const ScreenBuffer & world, const std::vector<Position> & food) 
{
	//choose a random direction and move
	int r = rand() % 4;
	Position dir;
	if (r == 0)
		dir = Position::UP;
	else if (r ==1)
		dir = Position::DOWN;
	else if (r ==2)
		dir = Position::LEFT;
	else if (r ==3)
		dir = Position::RIGHT;

	Position next(this->position);
	next.add(dir); // try to go here

	unsigned char terrain = world.get(next.y, next.x);

	if (terrain == GameWorld::WALL)
	{
		//do nothing
	} else if (terrain == GameWorld::TREE)
	{
		//create a path to the tree and step towards it
		currentPath.steps.push_back( next );
		state = FOLLOW_PATH;
	} else
	{
		//just walk there
		this->position.set(next.x, next.y);
	}

	//clamp to world bounds
	if (this->position.x < 0) this->position.x = 0;
	else if (this->position.x > world.getCols()) this->position.x = world.getCols()-1;

	if (this->position.y < 0) this->position.y = 0;
	else if (this->position.y > world.getRows()) this->position.y = world.getRows()-1;
}

/**
* performFollowPath(world, food)
*   - world: ScreenBuffer containing the game state, it is passed around to helper functions for AI
*   - food : Vector of Positions, the locations of food on the map, passed to help with AI
*   Goes to the next position in the currentPath. If it is in a tree, it takes 2 turns.
*/

void Elf::performFollowPath(const ScreenBuffer & world, const std::vector<Position> & food)
{
	if (this->currentPath.steps.size() > 0)
	{
		//check the next step
		Position next(this->currentPath.steps[0]);
		
		if (world.get(next.y, next.x) == GameWorld::TREE)
		{
			if (stepProgress < GameWorld::OBSTRUCTED)
			{
				//if walking into terrain, add to progress and wait for the next step
				stepProgress++;
				return;
			}
		}

		//if we're here, then we can move forward
		this->position.set(next.x, next.y);
		stepProgress = 0;
		currentPath.steps.erase(currentPath.steps.begin()); //clear out that step
	} else // path.steps.size == 0
	{
		//get a new path
		state = WANDER;
	}
} 

/**
* performPatrol(world, food)
*   - world: ScreenBuffer containing the game state, it is passed around to helper functions for AI
*   - food : Vector of Positions, the locations of food on the map, passed to help with AI
*   Not used.
*/
void Elf::performPatrol(const ScreenBuffer & world, const std::vector<Position> & food) {} 

#endif
