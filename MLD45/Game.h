#include "stdafx.h"
#include "GameWorld.h"
#include "GameObject.h"
#include "ScreenBuffer.h"
#include "Position.h"
#include "Creature.h"
#include "Elf.h"
#include "Path.h"

#ifndef _GAME_H_
#define _GAME_H_

//------------------------------------------------------------
//  Game class
//    This is the master class of the application. The run() method is all
//    that is needed to run the game.
//------------------------------------------------------------
class Game : 
	public GameObject
{
public:
	//constructors / destructor
	Game(void);
	~Game(void);

	//methods
	void run();


private:
	//member variables
	//char **screenBuffer;
	ScreenBuffer buffer;
	GameWorld world;

	std::vector<Position> treasure;
	std::vector<Position> food;
	std::vector<Elf> elves;

	void spawnFood();

	bool _isOver;
	bool _updateNeeded;

	//methods
	virtual void load();
	virtual void update();
	virtual void draw();
	virtual void unload();
	
	void flushBufferToScreen();
	void clearScreen();

	bool updateNeeded();
	bool isOver();

	bool stringMatch(const std::string &, const std::array<std::string, 4>&);
	bool stringMatch(const std::string &, const std::array<std::string, 3>&);
	void toLower(std::string & input);

	//constants
	static const std::array<std::string, 4> QUIT_STRINGS;
	static const std::array<std::string, 4> HELP_STRINGS;
	static const std::array<std::string, 3> ELF_STRINGS;
	static const std::array<std::string, 3> Game::GOBLIN_STRINGS;
	static const std::array<std::string, 3> Game::OGRE_STRINGS;
	static const std::array<std::string, 3> Game::BOMB_STRINGS;
	static const std::array<std::string, 3> Game::FOOD_STRINGS;
	static const std::array<std::string, 3> Game::TREASURE_STRINGS;

	//UI constants
	enum UI
	{
		BORDER_TOP_LEFT = 201,
		BORDER_TOP = 205,
		BORDER_TOP_RIGHT = 187,
		BORDER_SIDE = 186,
		BORDER_BOTTOM_LEFT = 200,
		BORDER_BOTTOM = 205,
		BORDER_BOTTOM_RIGHT = 188
	};
};

#endif