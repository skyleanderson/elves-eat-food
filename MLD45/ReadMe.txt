========================================================================
    CONSOLE APPLICATION : ElvesEatFood Project Overview
========================================================================

/////////////////////////////////////////////////////////////////////////////
Overview:

ElvesEatFood is a console application that runs a simple simulation of elves
wandering around and eating food. The map is randomly generated and the two
elves look for food.

This was motivated by the Mini Ludum Dare 45, the low-level game jam. I wanted
to make a pure c++ game with no external libraries or system code. These limitations
meant the only option was a turn based console application.

The original goal was to have a more robust sytem with 3 types of creatures
that would compete for food around the map. I did not have time to finish all
of that, so... the end result is a small simulation featuring the A* pathfinding.

/////////////////////////////////////////////////////////////////////////////
Run Instructions:

Windows - Run ElvesEatFood.exe
Other   - Build it and run it :)

/////////////////////////////////////////////////////////////////////////////
Controls:

Press Enter to perform one logic step.

Type 'quit' or 'q' to quit the application.

/////////////////////////////////////////////////////////////////////////////
Build Instructions:

Compile all *.cpp and *.h files into a single executable.
main.cpp contains the main() function.

/////////////////////////////////////////////////////////////////////////////
Credits:
Kyle Anderson - September 30 - Oct 4 2013
