#include "item.h"
#include "Path.h"

#ifndef _CREATURE_H_
#define _CREATURE_H_

//------------------------------------------------------------
//  Creature class
//    This is a virtual class used as the base for any creature in the game.
//    Currently, that just means elves..
//    The base implementations of the virtual methods set up the main AI
//    logic loop, and child classes just need to implement the chooseState() 
//    and performXX() methods.
//------------------------------------------------------------
class Creature :
	public Item
{
public:
	Creature(void);
	~Creature(void);


	//Constants
	enum ACTION_STATE
	{
		NONE,
		WANDER,
		FOLLOW_PATH,
		PATROL
	};

	//GameObject Interface
	virtual void update();
	virtual void update(const ScreenBuffer &, const std::vector<Position> &);
	virtual void draw() = 0;
	virtual void draw(ScreenBuffer &) = 0;

	//Member variables
	ACTION_STATE state;

	int health;
	int hunger;

	Path currentPath;

	//Member methods
	void damage(int value);
	bool isDead();


protected:

	//------------------------------------------------------------
	// These are the methods that child classes need to implement for AI behavior!!
	//------------------------------------------------------------
	virtual void chooseAction(const ScreenBuffer &, const std::vector<Position> &);
	virtual void performNone(const ScreenBuffer &, const std::vector<Position> &);
	virtual void performWander(const ScreenBuffer &, const std::vector<Position> &);
	virtual void performFollowPath(const ScreenBuffer &, const std::vector<Position> &);
	virtual void performPatrol(const ScreenBuffer &, const std::vector<Position> &);

};

#endif
