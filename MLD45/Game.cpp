#include "stdafx.h"
#include "Game.h"

#ifndef _GAME_CPP_
#define _GAME_CPP_


//------------------------------------------------------------
//  Constants
//------------------------------------------------------------
const std::array<std::string, 4> Game::QUIT_STRINGS     = {"quit",     "q",  "exit", "e"};
const std::array<std::string, 4> Game::HELP_STRINGS     = {"help",     "he", "h",    "?"};
const std::array<std::string, 3> Game::ELF_STRINGS      = {"elf",      "el", "e"};
const std::array<std::string, 3> Game::GOBLIN_STRINGS   = {"goblin",   "go", "g"};
const std::array<std::string, 3> Game::OGRE_STRINGS     = {"ogre",     "og", "o"};
const std::array<std::string, 3> Game::BOMB_STRINGS     = {"bomb",     "bo", "b"};
const std::array<std::string, 3> Game::FOOD_STRINGS     = {"food",     "fo", "f"};
const std::array<std::string, 3> Game::TREASURE_STRINGS = {"treasure", "tr", "t"};

//------------------------------------------------------------
// Constructors / Destructor
//------------------------------------------------------------

Game::Game(void)
{
}


Game::~Game(void)
{
}

//------------------------------------------------------------
//  Public Member Method
//------------------------------------------------------------

//PUBLIC
/**
* run()
*   The starting poitn for the game. This performs the game loop. It is designed so that the logic
*   and rendering are independent, but in this instance they are still called one to one.
*/
void Game::run()
{
	this->load();

	while (!this->isOver())
	{
		this->draw();

		while( this->updateNeeded() )
			this->update();
	} 

	this->unload();
}


//PRIVATE
//------------------------------------------------------------
//  GameObject Interface implementations
//------------------------------------------------------------
/**
* load()
*   Sets up the game.
*/
void Game::load() 
{
	//seed the random number generator
	srand(time(NULL));

	//world
	world.load();
	buffer.setBounds(GameWorld::WORLD_HEIGHT, GameWorld::WORLD_WIDTH); //row, col
	
	//food
	spawnFood();

	// temp two elves
	Elf elfOne;
	elfOne.load();
	elfOne.position = world.getRandomOpenPosition();
	elves.push_back( elfOne );

	Elf elfTwo;
	elfTwo.load();
	elfTwo.position = world.getRandomOpenPosition();
	elves.push_back( elfTwo );

	//init flags
	_isOver = false;
	_updateNeeded = true;
}

/**
* updateNeeded()
*   Returns true if the game logic needs to run. In a real-time game, this would calculate the time since the
*   last update.
*/
bool Game::updateNeeded() 
{
	return _updateNeeded;
}

/**
* update()
*   Performs the game logic. Calls the update() method for each object it owns.
*/
void Game::update() 
{
	_updateNeeded = false;
	std::string input;
	std::cout << ">> ";
	std::getline(std::cin, input);

	world.update(input);
	world.draw(buffer);

	//for each entity, tell entity to update
	for (unsigned int i = 0; i < elves.size(); i++)
	{
		elves[i].update(this->buffer, food);
	}

	//check if they got food
	for (unsigned int elf = 0; elf < elves.size(); elf++)
	{
		for (unsigned int f = 0; f < food.size(); f++)
		{
			if (elves[elf].position.x == food[f].x && elves[elf].position.y == food[f].y)
			{
				//feed the elf
				elves[elf].eat();
				
				//remove the food
				food.erase(food.begin() + f);

			}
		}
	}

	//if there is no food, spawn some more
	if (food.size() == 0)
	{
		spawnFood();
	}

	if (stringMatch(input, QUIT_STRINGS))
	{
		_isOver = true;
	}
}

/**
* draw()
*    Renders every object the game owns.  
*/

void Game::draw() 
{
	//fill the buffer
	//terrain
	world.draw(this->buffer);
	
	//fill in elves
	for (unsigned int i = 0; i < elves.size(); i++)
	{
		elves[i].draw(buffer);
	}
	
	//fill in food
	for (unsigned int i = 0; i < food.size(); i++)
	{
		buffer.set(food[i].y, food[i].x, GameWorld::FOOD);
	}
	//fill in treasure
	for (unsigned int i = 0; i < treasure.size(); i++)
	{
		buffer.set(treasure[i].y, treasure[i].x, GameWorld::TREASURE);
	}

	
	//flush the buffer to the screen
	flushBufferToScreen();

	_updateNeeded = true;
}

/**
* unload()
* Delete everything.
*/

void Game::unload() 
{
	world.unload();

	//delete all food objects
	while (!food.empty())
	{
		food.pop_back(); //will destroy the removed element
	}
}

//------------------------------------------------------------
//  Member Methods
//------------------------------------------------------------
/**
* flushBufferToScreen()
*   Adds the UI elements (borders, grid coordinates) and flushes the buffer to the screen.
*/
void Game::flushBufferToScreen()
{
	clearScreen();

	std::stringstream line;

	//draw top container lines
	line << ' ' << ' ';
	for (int col=0; col<70; col++)
	{
		line << col % 10;
	}
	line << std::endl;

	line << ' ' << char(Game::BORDER_TOP_LEFT);
	for (int col=0; col<70; col++)
	{
		if (col % 10 == 0)
			line << col / 10;
		else
			line << char(Game::BORDER_TOP);
	}
	line << char(Game::BORDER_TOP_RIGHT);
	line << std::endl;

	//draw the buffer inside the UI
	for (int row = 0; row < GameWorld::WORLD_HEIGHT; row++)
	{
		//left border
		line << char(row + 'a') << char(Game::BORDER_SIDE);

		line << this->buffer.getLine(row);

		//right border
		if (row % 3 == 0)
			line << char(row + 'a');
		else
			line << char(Game::BORDER_SIDE); 
		line << std::endl;
	}

	//draw the bottom border line
	line << ' ' << char(Game::BORDER_BOTTOM_LEFT);
	for (int col=0; col<70; col++)
	{
		if (col % 10 == 0)
			line << col / 10;
		else
			line << char(Game::BORDER_BOTTOM);
	}
	line << char(Game::BORDER_BOTTOM_RIGHT);
	line << std::endl;
	//flush line to screen
	std::cout << line.str();
}

/**
* clearScreen()
*   Outputs some blank lines to the screen
*/

void Game::clearScreen()
{
	for (int i=0; i < 5; i++)
		std::cout << std::endl;
}

/**
* isOver()
*   Lets the game loop in run() know that everything is done.
*/

bool Game::isOver()
{
	return _isOver;
}

/**
* spawnFood()
*   Adds two food items to the world.
*/
void Game::spawnFood()
{
	//put two foods randomly on the map
	food.emplace_back( world.getRandomOpenPosition() );
	food.emplace_back( world.getRandomOpenPosition() );
}

/**
* stringMatch(input, values)
*   - input : string
*   - values: vector of strings to match against the input
*   Returns true if input is in values.
*/
bool Game::stringMatch(const std::string & input, const std::array<std::string, 4> & values)
{
	for (int i=0; i < 4; i++)
	{
		if (input == values[i]) return true;
	}
	return false;
}

/**
* stringMatch(input, values)
*   - input : string
*   - values: vector of strings to match against the input
*   Returns true if input is in values.
*/bool Game::stringMatch(const std::string & input, const std::array<std::string, 3> & values)
{
	for (int i=0; i < 3; i++)
	{
		if (input == values[i]) return true;
	}
	return false;
}

/**
* toLower(input)
*   - input : string
*   Modifies string to be all lower case
*/void Game::toLower(std::string & input )
{
	char offset = 'a' - 'A';
	for (unsigned int i=0; i < input.length(); i++)
	{
		if (input[i] >= 'A' && input[i] <= 'Z')
		{
			input.replace(i, 1, 1, input[i] + offset);
		}
	}
}

#endif