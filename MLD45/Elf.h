#include "Creature.h"
#include "ScreenBuffer.h"

#ifndef _ELF_H_
#define _ELF_H_

//------------------------------------------------------------
//  Elf class
//    This implements the Creature virtual class. The only implemented
//    AI behaviors are WANDER and FOLLOW_PATH. The Path::getPathAStar()
//    method is required to get a path to follow.
//------------------------------------------------------------
class Elf :
	public Creature
{
public:
	Elf(void);
	~Elf(void);

	//Constants
	static const char DISPLAY_CHAR;
	static const int  BASE_HEALTH;
	static const int  BASE_HUNGER;
	static const int  HUNGER_LIMIT;

	//GameObject implementation
	virtual void load();
	virtual void update(const ScreenBuffer &, const std::vector<Position> &);
	virtual void draw();
	virtual void draw(ScreenBuffer & buffer);
	virtual void unload();

	//Member methods
	void eat();

	//Creature implementation
	virtual void chooseAction(const ScreenBuffer &, const std::vector<Position> &);
	virtual void performNone(const ScreenBuffer &, const std::vector<Position> &);
	virtual void performWander(const ScreenBuffer &, const std::vector<Position> &);
	virtual void performFollowPath(const ScreenBuffer &, const std::vector<Position> &);
	virtual void performPatrol(const ScreenBuffer &, const std::vector<Position> &);

private:
	//ai helpers
	void findFood(const ScreenBuffer &, const std::vector<Position> &);

	int stepProgress;
};

#endif

