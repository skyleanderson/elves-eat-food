#include "stdafx.h"

#ifndef _SCREEN_BUFFER_H_
#define _SCREEN_BUFFER_H_

//------------------------------------------------------------
//  ScreenBuffer class
//    This class (unfortunately) serves two purposes. First, it is used
//    by the Game class to cache the state of the world before drawing and to pass
//    to Creatures.  Second, it is used by GameWorld to store terrain data. It
//    should probably be renamed to CharMatrix based on its usage.
//------------------------------------------------------------
class ScreenBuffer
{
public:
	ScreenBuffer(void);
	ScreenBuffer(int, int);
	~ScreenBuffer(void);

	void setBounds(int rows, int cols); //set max width and height
	void setBounds(int rows, int cols, char initialValue); //set max width and height, pre-initialize
	void set(int row, int col, char c); //set an individual value
	char get(int row, int col) const; //get an individual value

	int getRows() const;
	int getCols() const;

	std::string getLine(int row); //get an entire line -- used for displaying everything
	
private:
	std::vector<char> _data;

	int _rows; //max rows
	int _cols; //max cols

	int indexOf(int row, int col) const; //helper to map a 2d array into a 1d array

};

#endif

