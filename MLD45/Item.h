#include "gameobject.h"
#include "Position.h"

#ifndef _ITEM_H_
#define _ITEM_H_

//------------------------------------------------------------
//  Item class
//    This was intended to be a base class for anything that lives
//    in the gameworld.. It is not particularly useful and probably
//    could be consumed into Creature.
//------------------------------------------------------------
class Item :
	public GameObject
{
public:
	Item(void);
	~Item(void);

	Position position;

};

#endif