#ifndef _GAMEWORLD_H_
#define _GAMEWORLD_H_

#include "GameObject.h"
#include "ScreenBuffer.h"
#include "Item.h"

//------------------------------------------------------------
//  GameWorld class
//    GameWorld holds the terrain data of the game. It also defines a few
//    constants and helpers for other objects to look at the map.
//------------------------------------------------------------
class GameWorld : 
	public GameObject
{
public:
	GameWorld(void);
	~GameWorld(void);

	//Constants
	static const int WORLD_WIDTH;
	static const int WORLD_HEIGHT;

	//helper functions for path-finding
	enum WALKING_STEPS
	{
		UNWALKABLE = -1,
		WALKABLE = 1,
		OBSTRUCTED = 2,
	};

	//terrain constants
	enum TERRAIN_TYPE
	{
		LAND = ' ',
		TREE = 241,
		WALL = 178,
	};

	enum ITEMS
	{
		FOOD = 'f',
		TREASURE = 't',
	};

	enum ENEMY_TYPE
	{
		ELF = 'E',
		GOBLIN = 'g',
		GOBLIN_STACKED = 'G',
		OGRE = 'O',
		OGRE_ASLEEP = 'o',
	};

	//Static Methods
	static WALKING_STEPS TerrainSteps(const ScreenBuffer & state, const Position & location);
	static bool IsPositionInBounds(const Position & location);


	//implement GameObject
	virtual void load();
	virtual void update();
	void update(std::string input);
	virtual void draw();
	void draw(ScreenBuffer & buffer);
	virtual void unload();

	//Member Methods
	//helper for rendering and interacting with the world
	char get(int row, int col) const; //get an individual value -- entity if it sits there, or terrain otherwise
	Position getRandomOpenPosition();

	//Member Variables
	ScreenBuffer terrain;
};

#endif